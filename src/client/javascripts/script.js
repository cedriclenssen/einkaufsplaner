let userid;
const today = new Date();
let listDate = today;
const date = getDateInSQLFormat(today);
let weekid = getWeekid(today);
const generalurl = "https://einkaufsplaner.azurewebsites.net/";
//const generalurl = 'http://localhost:8080/';
let userstatus;

/* 
wird beim ersten Laden der Seite ausgeführt
*/
window.onload = async function () {
  await load();
  disableUsableElems();
};

/* 
wird alle 5 Sekunden ausgeführt
*/
setInterval(function () {
  reload(weekid);
}, 5000);

/* 
Die Funktion führt alle Funktionen aus, die beim Laden der Seite ausgeführt werden müssen
*/
async function load() {
  headerSetNextMeeting(today);
  await getList(weekid);
  checkDate(weekid);
}

/* 
Die Funktion führt alle Funktionen aus, die beim Aktualisieren der Seite ausgeführt werden müssen
*/
async function reload(weekid) {
  await getBuyersJS(weekid);
  await getList(weekid);
}

/* 
Die Funktion behandelt die Funktionalität des "Woche zurück"-Buttons
*/
function weekBack() {
  listDate = new Date(
    listDate.getFullYear(),
    listDate.getMonth(),
    listDate.getDate() - 7,
    listDate.getHours(),
    listDate.getMinutes(),
    listDate.getSeconds()
  );
  weekid = getWeekid(listDate);
  deleteList();
  reload(weekid);
  headerSetNextMeeting(listDate);
  checkDate(weekid);
}

/* 
Die Funktion behandelt die Funktionalität des "Woche vor"-Buttons
*/
function weekForward() {
  listDate = new Date(
    listDate.getFullYear(),
    listDate.getMonth(),
    listDate.getDate() + 7,
    listDate.getHours(),
    listDate.getMinutes(),
    listDate.getSeconds()
  );
  weekid = getWeekid(listDate);
  deleteList();
  reload(weekid);
  headerSetNextMeeting(listDate);
  checkDate(weekid);
}

/*
Die Funktion entfernt alle aktuell angezeigten Einträge aus dem HTML-Dokument
*/
function deleteList() {
  const liste = document.getElementById("list-items");
  const marked = Array.prototype.slice.call(
    document.getElementsByClassName("entry-marked")
  );
  const unmarked = Array.prototype.slice.call(
    document.getElementsByClassName("entry-unmarked")
  );

  for (let elem of marked) {
    liste.removeChild(elem);
  }
  for (let elem of unmarked) {
    liste.removeChild(elem);
  }
}

/*
Die Funktion behandelt die Funktionalität des hinzufügen-Buttons
*/
async function addArticle() {
  const description = document.getElementById("description-input").value;
  let amountString = document.getElementById("amount-input").value;
  const unit = document.getElementById("unit-input").value;

  //Kontrolle der Eingabeparameter
  if (description == "" || amountString == "" || unit == "") {
    alert("Bitte alle Felder ausfüllen!");
    return;
  }

  amountString = amountString.replaceAll(",", ".");
  const amount = parseInt(amountString);
  if (!Number.isInteger(amount) || amount != parseFloat(amountString)) {
    alert("Menge muss eine ganze Zahl sein!");
    return;
  }
  if (description.length > 100) {
    alert("Eintrag darf maximal 100 Zeichen enthalten!");
    return;
  }
  if (unit.length > 40) {
    alert("Einheit darf maximal 100 Zeichen enthalten!");
    return;
  }

  //Absenden der Werte per HTTP Request
  const url = generalurl + "item" + "/";
  let data = {
    description: description,
    amount: amount,
    unit: unit,
    userid: userid,
    date: date,
    weekid: weekid,
  };
  var request = new Request(url, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });

  getList(weekid);

  //Leeren der Eingabefelder
  document.getElementById("description-input").value = "";
  document.getElementById("amount-input").value = "";
  document.getElementById("unit-input").value = "";
}

/*
Die Funktion ruft alle Listeneinträge der jeweiligen Woche aus der Datenbank ab
und fügt sie zur HTML-Datei hinzu
*/
async function getList(weekid) {
  const url = generalurl + "list/" + weekid + "/";
  let list;

  //Empfang der Daten aus der Datenbank
  var request = new Request(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    list = await res.json();
  });

  const marked = await getMarked(weekid);
  let listOfMarkedEntryIds = [];
  marked.forEach((element) => {
    listOfMarkedEntryIds.push(element.entryid);
  });

  const divForItems = document.getElementById("list-items");
  //Hinzufügen der Elemente zu index.html

  list.forEach((element) => {
    const isMarked = listOfMarkedEntryIds.includes(element.entryid);
    const indexMarked = listOfMarkedEntryIds.indexOf(element.entryid);

    divItem = document.createElement("div");
    divItem.setAttribute("id", "" + element.entryid);
    if (isMarked) {
      divItem.setAttribute("data-marked", true);
      divItem.setAttribute("class", "entry-marked");
    } else {
      divItem.setAttribute("class", "entry-unmarked");
      divItem.setAttribute("data-marked", false);
    }

    divFullEntryInfo = document.createElement("div");
    divFullEntryInfo.setAttribute("class", "full-entry-info");
    divFullEntryInfo.setAttribute("id", "full-entry-info-" + element.entryid);
    divEntryInfo = document.createElement("div");
    divEntryInfo.setAttribute("class", "entry-info");
    divEntryInfo.setAttribute("id", "entry-info-" + element.entryid);

    spanDescription = document.createElement("span");
    spanDescription.setAttribute("class", "description");
    spanDescription.setAttribute("id", "description" + element.entryid);
    spanDescription.innerText = element.description;
    if (isMarked) {
      spanDescription.setAttribute("class", "description-marked");
    } else {
      spanDescription.setAttribute("class", "description-unmarked");
    }
    spanAmount = document.createElement("span");
    spanAmount.setAttribute("class", "amount");
    spanAmount.setAttribute("id", "amount" + element.entryid);
    spanAmount.innerText = element.amount;
    spanUnit = document.createElement("span");
    spanUnit.setAttribute("class", "unit");
    spanUnit.setAttribute("id", "unit" + element.entryid);
    spanUnit.innerText = element.unit;
    divEntryInfo.appendChild(spanDescription);
    divEntryInfo.appendChild(spanAmount);
    divEntryInfo.appendChild(spanUnit);

    divFullEntryInfo.appendChild(divEntryInfo);

    divUserInfo = document.createElement("div");
    divUserInfo.setAttribute("class", "user-entry-info");
    divUserInfo.setAttribute("id", "user-entry-info-" + element.entryid);

    spanAdded = document.createElement("span");
    spanAdded.setAttribute("id", "added-" + element.entryid);
    if (isMarked) {
      spanAdded.innerText = "gekauft von ";
    } else {
      spanAdded.innerText = "hinzugefügt von ";
    }
    spanBy = document.createElement("span");
    spanBy.setAttribute("id", "by-" + element.entryid);
    if (isMarked) {
      spanBy.innerText =
        formatName(marked[indexMarked].prename) +
        " " +
        formatName(marked[indexMarked].name) +
        " ";
    } else {
      spanBy.innerText =
        formatName(element.prename) + " " + formatName(element.name) + " ";
    }
    spanOn = document.createElement("span");
    spanOn.setAttribute("id", "on-" + element.entryid);
    spanOn.innerText = "am ";
    spanDate = document.createElement("span");
    spanDate.setAttribute("id", "date-" + element.entryid);
    if (isMarked) {
      spanDate.innerText = "" + sqlToDisplayDate(marked[indexMarked].markDate);
    } else {
      spanDate.innerText = sqlToDisplayDate("" + element.creationDate);
    }

    divUserInfo.appendChild(spanAdded);
    divUserInfo.appendChild(spanBy);
    divUserInfo.appendChild(spanOn);
    divUserInfo.appendChild(spanDate);

    divFullEntryInfo.appendChild(divUserInfo);
    divItem.appendChild(divFullEntryInfo);

    divButtons = document.createElement("div");
    divButtons.setAttribute("class", "button-bereich");

    buttonPencil = document.createElement("button");
    iconPencil = document.createElement("i");
    iconPencil.setAttribute("class", "bi bi-pencil");
    buttonPencil.appendChild(iconPencil);
    buttonPencil.setAttribute(
      "onclick",
      "editEntry(this.parentElement.parentElement)"
    );
    buttonPencil.setAttribute("disabled", "disabled");
    if (userid == element.userid) {
      buttonPencil.removeAttribute("disabled");
    }

    buttonTrash = document.createElement("button");
    iconTrash = document.createElement("i");
    iconTrash.setAttribute("class", "bi bi-trash");
    buttonTrash.appendChild(iconTrash);
    buttonTrash.setAttribute(
      "onclick",
      "deletePopup(this.parentElement.parentElement)"
    );
    buttonTrash.setAttribute("disabled", "disabled");
    if (userid == element.userid) {
      buttonTrash.removeAttribute("disabled");
    }

    buttonCart = document.createElement("button");
    iconCart = document.createElement("i");
    iconCart.setAttribute("class", "bi bi-cart");
    buttonCart.appendChild(iconCart);
    buttonCart.setAttribute("class", "mark");
    buttonCart.setAttribute("disabled", "disabled");
    buttonCart.setAttribute(
      "onclick",
      "markEntryJS(this.parentElement.parentElement)"
    );
    if (userstatus == "buyer") {
      buttonCart.removeAttribute("disabled");
    }

    divButtons.appendChild(buttonPencil);
    divButtons.appendChild(buttonTrash);
    divButtons.appendChild(buttonCart);

    divItem.appendChild(divButtons);

    if (document.getElementById("" + element.entryid) == null) {
      divForItems.appendChild(divItem);
    } else {
      divForItems.replaceChild(
        divItem,
        document.getElementById("" + element.entryid)
      );
    }
  });
}

/*
Die Funktion ruft alle markierten Listeneinträge aus der Datenbank ab und färbt sie entsprechend ein.
*/
async function getMarked(weekid) {
  const url = generalurl + "marked/" + weekid + "/";
  let list;

  //Empfang der Daten aus der Datenbank
  var request = new Request(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    list = await res.json();
  });

  return list;
}

/*
Die Funktion löscht einen Eintrag aus der Liste
*/
async function deleteEntry(elem) {
  removeWindow(elem);
  const entryid = parseInt(elem.id.split("-")[1]);
  const entry = document.getElementById(entryid);
  //Entfernen des Eintrags aus der Liste, die angezeigt wird
  document.getElementById("list-items").removeChild(entry);

  //Entfernen des Eintrags aus der Datenbank
  const url = generalurl + "delete" + "/";
  let data = {
    entryid: entryid,
  };
  var request = new Request(url, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });
}

async function deletePopup(elem) {
  const container = document.getElementById("edit-container");
  const elemId = elem.getAttribute("id");

  const divEditWindow = document.createElement("div");
  divEditWindow.setAttribute("class", "edit-window");
  divEditWindow.setAttribute("id", "delete-" + elemId);

  const divEditContents = document.createElement("div");
  divEditContents.setAttribute("class", "edit-contents");
  divEditContents.setAttribute("id", "edit-contents");

  const confirmationSpan = document.createElement("div");
  confirmationSpan.innerText = "Möchtest du den Eintrag wirklich löschen?";

  const addButton = document.createElement("button");
  addButton.setAttribute("class", "button-primary");
  addButton.innerText = "Eintrag löschen";
  addButton.setAttribute(
    "onclick",
    "deleteEntry(this.parentElement.parentElement)"
  );

  const cancelButton = document.createElement("button");
  cancelButton.setAttribute("class", "button-secondary");
  cancelButton.innerText = "Abbrechen";
  cancelButton.setAttribute(
    "onclick",
    "removeWindow(this.parentElement.parentElement)"
  );

  divEditContents.appendChild(confirmationSpan);
  divEditContents.appendChild(document.createElement("br"));
  divEditContents.appendChild(addButton);
  divEditContents.appendChild(cancelButton);

  divEditWindow.appendChild(divEditContents);

  container.appendChild(divEditWindow);
  container.setAttribute("style", "visibility:visible; display:block");
}

/*
Die Funktion behandelt die Funktion des (De-)Markierens
*/
async function markEntryJS(elem) {
  if (elem.getAttribute("class") == "entry-unmarked") {
    //TODO wenn Eintrag markiert werden soll

    const url = generalurl + "mark" + "/";
    let data = {
      entryid: parseInt(elem.getAttribute("id")),
      userid: userid,
      date: date,
    };
    var request = new Request(url, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    await fetch(request).then(async (res) => {
      await res.text();
    });

    await getList(weekid);
    await getMarked(weekid);
  } else {
    //TODO wenn Eintrag demarkiert werden soll
    const url = generalurl + "demark" + "/";
    let data = {
      entryid: parseInt(elem.getAttribute("id")),
    };
    var request = new Request(url, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    await fetch(request).then(async (res) => {
      await res.text();
    });
    await getList(weekid);
    await getMarked(weekid);
  }
}

/*
Die Funktion erstellt das Editierfenster
*/
function editEntry(elem) {
  const container = document.getElementById("edit-container");
  const elemId = elem.getAttribute("id");
  const descriptionValue = document.getElementById(
    "description" + elemId
  ).innerText;
  const amountValue = document.getElementById("amount" + elemId).innerText;
  const unitValue = document.getElementById("unit" + elemId).innerText;

  const divEditWindow = document.createElement("div");
  divEditWindow.setAttribute("class", "edit-window");
  divEditWindow.setAttribute("id", "edit-" + elemId);

  const divEditContents = document.createElement("div");
  divEditContents.setAttribute("class", "edit-contents");
  divEditContents.setAttribute("id", "edit-contents");

  const description = document.createElement("input");
  description.setAttribute("type", "text");
  description.setAttribute("class", "description-input-edit");
  description.setAttribute("id", "description-input-edit");
  description.setAttribute("placeholder", descriptionValue);

  const amount = document.createElement("input");
  amount.setAttribute("type", "text");
  amount.setAttribute("class", "amount-input-edit");
  amount.setAttribute("id", "amount-input-edit");
  amount.setAttribute("placeholder", amountValue);

  const unit = document.createElement("input");
  unit.setAttribute("type", "text");
  unit.setAttribute("class", "unit-input-edit");
  unit.setAttribute("id", "unit-input-edit");
  unit.setAttribute("placeholder", unitValue);

  const addButton = document.createElement("button");
  addButton.setAttribute("class", "button-primary");
  addButton.innerText = "Änderungen speichern";
  addButton.setAttribute(
    "onclick",
    "updateElementJS(this.parentElement.parentElement)"
  );

  const cancelButton = document.createElement("button");
  cancelButton.setAttribute("class", "button-secondary");
  cancelButton.innerText = "Abbrechen";
  cancelButton.setAttribute(
    "onclick",
    "removeWindow(this.parentElement.parentElement)"
  );

  divEditContents.appendChild(description);
  divEditContents.appendChild(document.createElement("br"));
  divEditContents.appendChild(amount);
  divEditContents.appendChild(unit);
  divEditContents.appendChild(document.createElement("br"));
  divEditContents.appendChild(addButton);
  divEditContents.appendChild(cancelButton);

  divEditWindow.appendChild(divEditContents);

  container.appendChild(divEditWindow);
  container.setAttribute("style", "visibility:visible; display:block");
}

/*
Die Funktion aktualisiert den Datenbankeintrag des geänderten Artikels
*/
async function updateElementJS(windowElem) {
  const entryidArray = windowElem.getAttribute("id").split("-");
  const entryid = entryidArray[1];
  let description = document.getElementById("description-input-edit").value;
  let amountString = document.getElementById("amount-input-edit").value;
  let unit = document.getElementById("unit-input-edit").value;

  if (description == "" && amountString == "" && unit == "") {
    removeWindow(windowElem);
    return;
  }
  if (description == "") {
    description = document
      .getElementById("description-input-edit")
      .getAttribute("placeholder");
  }
  if (amountString == "") {
    amountString = document
      .getElementById("amount-input-edit")
      .getAttribute("placeholder");
  }
  if (unit == "") {
    unit = document
      .getElementById("unit-input-edit")
      .getAttribute("placeholder");
  }

  const amount = parseInt(amountString);
  if (!Number.isInteger(amount)) {
    alert("Menge muss eine ganze Zahl sein!");
    return;
  }
  if (description.length > 100) {
    alert("Eintrag darf maximal 100 Zeichen enthalten!");
    return;
  }
  if (unit.length > 40) {
    alert("Einheit darf maximal 100 Zeichen enthalten!");
    return;
  }

  const url = generalurl + "item" + "/";
  let data = {
    entryid: parseInt(entryid),
    description: description,
    amount: amount,
    unit: unit,
  };
  var request = new Request(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });

  getList(weekid);
  removeWindow(windowElem);
}

/*
Die Funktion erstellt die Liste der Einkäufer:innen
*/
async function getBuyersJS(weekid) {
  const url = generalurl + "buyerlist/" + weekid + "/";
  let list;

  //Empfang der Daten aus der Datenbank
  var request = new Request(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    list = await res.json();
  });

  const divBuyers = document.getElementById("buyer-list");
  const arrayOfBuyerIds = [];

  while (divBuyers.firstChild) {
    divBuyers.removeChild(divBuyers.lastChild);
  }

  list.forEach((element) => {
    const li = document.createElement("div");
    li.setAttribute("class", "list-item");
    li.setAttribute("id", "buyer-" + element.userid);

    const span = document.createElement("span");
    span.innerText = element.prename + " " + formatName(element.name);

    li.appendChild(span);
    li.appendChild(document.createElement("br"));

    if (document.getElementById("buyer" + element.userid) == null) {
      divBuyers.appendChild(li);
    } else {
      divBuyers.replaceChild(
        li,
        document.getElementById("buyer" + userid.entryid)
      );
    }

    arrayOfBuyerIds.push(element.userid);
  });

  if (arrayOfBuyerIds.length == 0) {
    const li = document.createElement("div");
    li.setAttribute("class", "list-item");

    const span = document.createElement("span");
    span.innerText = "keine Einkäufer:innen";

    li.appendChild(span);
    li.appendChild(document.createElement("br"));
    divBuyers.appendChild(li);
  }

  const buyerButton = document.getElementById("buyer-button");
  const roleDisplay = document.getElementById("role");
  if (arrayOfBuyerIds.includes(userid)) {
    buyerButton.innerText = "Ich möchte nicht mehr einkaufen!";
    userstatus = "buyer";
    buyerButton.setAttribute("onclick", "becomeWorkerJS()");
    roleDisplay.innerText = "Einkäufer:in!";
  } else {
    buyerButton.innerText = "Ich kaufe für das nächste Meeting ein";
    userstatus = "worker";
    buyerButton.setAttribute("onclick", "becomeBuyerJS()");
    roleDisplay.innerText = "Mitarbeiter:in!";
  }
}

/*
Die Funktion ändert den Status des:der Mitarbeiter:in zum:zur Einkäufer:in
*/
async function becomeBuyerJS() {
  const url = generalurl + "buyer" + "/";
  //Empfang der Daten aus der Datenbank
  let data = {
    weekid: weekid,
    userid: userid,
  };
  var request = new Request(url, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });
  reload(weekid);
}

/* 
Die Funktion ändert den Status des:der Einkäufer:in zum:zur Mitarbeiter:in
*/
async function becomeWorkerJS() {
  const url = generalurl + "worker" + "/";
  //Empfang der Daten aus der Datenbank
  let data = {
    weekid: weekid,
    userid: userid,
  };
  var request = new Request(url, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });
  reload(weekid);
}

/*
Die Funktion behandelt die Funktionalität des Anmeldebuttons
*/
async function login() {
  const username = document.getElementById("username").value;
  const password = document.getElementById("password").value;

  if (username == "" || password == "") {
    alert("Der Benutzername oder das Passwort sind falsch!");
    return;
  }

  const url = generalurl + "login/" + username + "/" + password + "/";
  let response;

  //Empfang der Daten aus der Datenbank
  var request = new Request(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    response = await res.text();
  });

  if (response == "valid") {
    userid = username;
    loadUsablePage(userid);
  } else {
    alert("Der Benutzername oder das Passwort sind falsch!");
  }
}

/*
Die Funktion lädt die Seite nach der Anmeldung
*/
async function loadUsablePage(userid) {
  const response = await getUserinfoJS(userid);
  document.getElementById("welcome-message").innerText =
    "Hallo " + response[0].prename + "!";
  document
    .getElementById("information")
    .setAttribute("class", "visible information");
  document.getElementById("login").setAttribute("class", "login invisible");
  const list = document.getElementsByClassName("usable");
  for (let elem of list) {
    elem.removeAttribute("disabled");
  }
  reload(weekid);
}

/*
Die Funktion lädt die Userinformationen aus der Datenbank
*/
async function getUserinfoJS(userid) {
  const url = generalurl + "userinfo/" + userid + "/";
  let response;

  //Empfang der Daten aus der Datenbank
  var request = new Request(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    response = await res.json();
  });
  return response;
}

/*
Die Funktion deaktiviert Buttons und Inputfelder
*/
function disableUsableElems() {
  const list = document.getElementsByClassName("usable");
  for (let elem of list) {
    elem.setAttribute("disabled", "disabled");
  }
}

/*
Wandelt ein Datum in gültiges SQL-Date-Format um
*/
function getDateInSQLFormat(date) {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const returnString = "" + year + "-" + month + "-" + day;
  return returnString;
}

/*
Gibt die weekid des nächsten Teammeetings zurück. Also am Dienstag der KW1 im Jahr 2022 wird '22022' zurückgegeben
*/
function getWeekid(date) {
  let year = date.getFullYear();
  let week = getWeek(date);
  let meetingTime = new Date(date);
  meetingTime.setHours(12);
  meetingTime.setMinutes(0);
  meetingTime.setSeconds(0);
  meetingTime.setUTCMilliseconds(0);
  if (date.getDay() == 1 && date < meetingTime) {
    if (week < 10) {
      week = "0" + week;
    }
    return "" + week + year;
  } else {
    week++;
    if (week < 10) {
      week = "0" + week;
    }
    if (week == 53) {
      week = "01";
      year++;
    }
    return "" + week + year;
  }
}

/*
Gibt die Nummer der Kalenderwoche eines Datums zurück
*/
function getWeek(year, month, day) {
  function serial(days) {
    return 86400000 * days;
  } //berechnet Anzahl ms an days Tagen
  function dateserial(year, month, day) {
    return new Date(year, month - 1, day).valueOf();
  } //gibt ms seit 1970 von angegeben Datum zurück
  function weekday(date) {
    return new Date(date).getDay() + 1;
  } //gibt nächsten Wochentag
  function yearserial(date) {
    return new Date(date).getFullYear();
  }
  var date =
      year instanceof Date
        ? year.valueOf()
        : typeof year === "string"
        ? new Date(year).valueOf()
        : dateserial(year, month, day),
    date2 = dateserial(
      yearserial(date - serial(weekday(date - serial(1))) + serial(4)),
      1,
      3
    );
  return ~~((date - date2 + serial(weekday(date2) + 5)) / serial(7));
}

/*
Die Funktion setzt das Datum im Header auf das Datum des nächsten Meetings
*/
function headerSetNextMeeting(dateGiven) {
  const date = new Date(dateGiven);
  let day;
  const meetingTime = new Date(date);
  meetingTime.setHours(12);
  meetingTime.setMinutes(0);
  meetingTime.setSeconds(0);
  meetingTime.setUTCMilliseconds(0);
  if (date.getDay() == 1 && date < meetingTime) {
    day = date.getDate();
  } else {
    let delta = (7 - date.getDay() + 1) % 7;
    if (delta == 0) {
      delta = 7
    }
    date.setDate(date.getDate() + delta);
    day = date.getDate();
  }
  if (day < 10) {
    day = "0" + day;
  }
  const monthNames = [
    "Januar",
    "Februar",
    "März",
    "April",
    "Mai",
    "Juni",
    "Juli",
    "August",
    "September",
    "Oktober",
    "November",
    "Dezember",
  ];
  var datum = document.getElementById("datum");
  let month = date.getMonth();
  const year = date.getFullYear();
  month = monthNames[month];
  datum.innerText = day + ". " + month + " " + year;
}

/*
Die Funktion wandelt das Datum vom SQL-Format in das Anzeigeformat um
*/
function sqlToDisplayDate(datetimeString) {
  const monthNames = [
    "Januar",
    "Februar",
    "März",
    "April",
    "Mai",
    "Juni",
    "Juli",
    "August",
    "September",
    "Oktober",
    "November",
    "Dezember",
  ];
  let dateString = datetimeString.split("T");
  let dateElements = dateString[0].split("-");
  let resultString =
    dateElements[2] + ". " + monthNames[parseInt(dateElements[1]) - 1];
  return resultString;
}

/*
Die Funktion kontrolliert, ob ein neuer Eintrag in der week-Tabelle der Datenbank hinzugefügt werden muss
*/
async function checkDate(weekid) {
  const url = generalurl + "week/" + weekid + "/";
  let weekEntry;
  var request = new Request(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    weekEntry = await res.json();
  });

  if (weekEntry.length == 0) {
    addWeek(weekid);
  }
}

/*
Die Funktion fügt der einen neuen Eintrag in die week-Tabelle der Datenbank 
*/
async function addWeek(weekid) {
  const url = generalurl + "week" + "/";
  let data = {
    weekid: weekid,
    week: weekid.substring(0, 2),
    year: weekid.substring(2),
  };
  var request = new Request(url, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  await fetch(request).then(async (res) => {
    await res.text();
  });
}

/*
Die Funktion entfernt das Editierfenster
*/
function removeWindow(elem) {
  document.getElementById("edit-container").removeChild(elem);
  document
    .getElementById("edit-container")
    .setAttribute("style", "visibility:hidden; display:none");
}

/*
Die Funktion formatiert bestimmte Namen mit Sonderzeichen
*/
function formatName(name) {
  if (name == "Kubovcik") {
    name = "Kubovčík";
  }
  return name;
}
