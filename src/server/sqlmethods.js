const { Connection, Request } = require("tedious");
var TYPES = require('tedious').TYPES

//Konfiguration erzeugen
const config = {
    authentication: {
      options: {
        userName: "CloudSAfeaacb41", // update me
        password: "raevoluz-admin2021" // update me
      },
      type: "default"
    },
    server: "raevoluz-einkaufsliste-database.database.windows.net", // update me
    options: {
      database: "raevoluz-einkaufsliste-database", //update me
      encrypt: true
    }
  };



/*
Die folgenden Methoden verbinden sich mit der Datenbank und rufen die
ihnen entsprechende 'Query-Methode' auf.
*/


function getUsername(userid, result) {

  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      getUsernameSQL(connection, userid, function(error, results) {
        if(error){
          return result(error);
        } else {
          result(null, results);
        }
      })   
    }
  });
}

function addEntry(description, amount, unit, userid, date, weekid, result) {
  
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      result(err);
    } else {
      addEntrySQL(connection, description, amount, unit, userid, date, weekid, function(error, results) {
        if(error){

        } else {
          result(null, results);
        }
      });
    }
  });
}

function updateEntry(entryid, description, amount, unit, result) {
  
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
      result(err);
    } else {
      updateEntrySQL(connection, entryid, description, amount, unit, function(error, results) {
        if(error){

        } else {
          result(null, results);
        }
      });
    }
  });
}

function deleteEntry(entryid) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      deleteEntrySQL(connection,entryid);
    }
  });
}

function markEntry(entryid, userid, markDate) {

  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      markEntrySQL(connection, entryid, userid, markDate);
    }
  });
}

function demarkEntry(entryid) {

  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      demarkEntrySQL(connection, entryid);
    }
  });
}

function becomeBuyer(userid, weekid) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      becomeBuyerSQL(userid, weekid);
    }
  });

}

function becomeWorker(userid, weekid) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      becomeWorkerSQL(userid, weekid);
    }
  });

}

function getList(weekid, result) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      getListSQL(connection, weekid, function(error, results) {
        if(error){

        } else {
          result(null, results);
        }
      });   
    }
  });

}

function createWeek(weekid, week, year) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      createWeekSQL(connection, weekid, week, year);
    }
  });
}

function getBuyers(weekid) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      getBuyersSQL(weekid);
    }
  });
}

function getWeekEntry(weekid, result) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      getWeekEntrySQL(connection, weekid, function(error, results) {
        if(error){

        } else {
          result(null, results);
        }
      });   
    }
  });

}

function getMarkedEntries(weekid, result) {
  const connection = new Connection(config);
  connection.connect();
  //Verbindung herstellen
  connection.on("connect", err => {
    if (err) {
      console.error(err.message);
    } else {
      getMarkedEntriesSQL(connection, weekid, function(error, results) {
        if(error){

        } else {
          result(null, results);
        }
      });   
    }
  });

}
//Die folgenden Methoden implementieren die SQL-Queries.

/* 
Funktion, um übergebene userid aus der Datenbank zu bekommen,
wenn sie vorhanden ist.
*/
function getUsernameSQL(connection, userid, results) {

  let returnArray = [];

  const request = new Request(
    `SELECT userid from [user] where userid = '${userid}'`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
        return results(error);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
        results(null, returnArray);
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      returnArray.push(column.value);
    });
  });

  connection.execSql(request);
}

/*
Funktion, um einen Eintrag hinzuzufügen. Entsprechende Daten werden in
entry, creatortoentry und weektoentry eingetragen
*/
function addEntrySQL(connection, description, amount, unit, userid, date, weekid, results) {

  const request = new Request(
    `INSERT INTO entry ([description], amount, unit) values('${description}', ${amount}, '${unit}');
    INSERT INTO creatortoentry values('${userid}', (SELECT max(entryid) from entry), '${date}');
    INSERT INTO entrytoweek values((SELECT max(entryid) from entry), '${weekid}')`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
        return results(err);
      } else {
        console.log(`${rowCount} row(s) returned`);
        results(null, true);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/*
Funktion, um einen bestehenden Artikel in der Datenbank zu ändern.
*/
function updateEntrySQL(connection, entryid, description, amount, unit, results) {

  const request = new Request(
    `UPDATE entry SET description = '${description}', amount = ${amount}, unit = '${unit}' 
    where entryid = '${entryid}';`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
        return results(err);
      } else {
        console.log(`${rowCount} row(s) returned`);
        results(null, true);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/*
Die Funktion löscht einen Eintrag aus der Datenbank
*/
function deleteEntrySQL(connection, entryid) {

  const request = new Request(
    `DELETE from creatortoentry WHERE entryid = '${entryid}';
    DELETE from buyertoentry WHERE entryid = '${entryid}';
    DELETE from entrytoweek WHERE entryid = '${entryid}';
    DELETE from entry WHERE entryid = '${entryid}';
    `,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/*
Die Funktion fügt einen Eintrag in buyertoentry hinzu, sodass ein Eintrag als markiert angesehen wird.
*/
function markEntrySQL(connection, entryid, userid, markDate) {

  const request = new Request(
    `INSERT INTO buyertoentry values('${userid}', '${entryid}', '${markDate}');`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/*
Die Funktion löscht einen Eintrag aus buyertoentry, sodass ein Eintrag nicht mehr als markiert angesehen wird.
*/
function demarkEntrySQL(connection, entryid) {
  const request = new Request(
    `DELETE FROM buyertoentry WHERE entryid = '${entryid}';`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/* 
Die Funktion fügt einen Eintrag in buyertoweek hinzu, sodass die entsprechende Person als Einkäufer:in angesehen wird.
*/
function becomeBuyerSQL(connection, userid, weekid) {

  const request = new Request(
    `INSERT INTO buyertoweek values('${userid}', '${weekid}');`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/* 
Die Funktion löscht einen Eintrag aus buyertoweek, sodass die entsprechende Person nicht mehr als Einkäufer:in angesehen wird.
*/
function becomeWorkerSQL(connection, userid, weekid) {

  const request = new Request(
    `DELETE FROM buyertoweek where userid = '${userid}' AND weekid = '${weekid}';`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/* 
Die Funktion gibt alle Einträge aus entry mit dem:der Ersteller:in, der entsprechenden Woche zurück.
*/
function getListSQL(connection, weekid, results) {

  returnArray = [];

  const request = new Request(
    `SELECT entry.entryid, [description], amount, unit, creationDate, [name], prename 
    from entry INNER JOIN creatortoentry
    on (entry.entryid = creatortoentry.entryid)
    INNER JOIN [user] on (creatortoentry.userid = [user].userid)
    INNER JOIN entrytoweek on (entry.entryid = entrytoweek.entryid)
    WHERE weekid = '${weekid}';`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
        return results(err);
      } else {
        console.log(`${rowCount} row(s) returned`);
        results(null, returnArray);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    let returnLine = []
    columns.forEach(column => {
      returnLine.push(column.value);
    });
    returnArray.push(returnLine);
  });

  connection.execSql(request);
}

/*
Die Funktion erstellt einen Eintrag in der week-Tabelle.
*/
function createWeekSQL(connection, weekid, week, year) {

  const request = new Request(
    `INSERT INTO week values('${weekid}', ${week}, ${year});`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      console.log("%s\t%s", column.metadata.colName, column.value);
    });
  });

  connection.execSql(request);
}

/* 
Die Funktion gibt alle Einkäufer dieser Woche zurück.
*/
function getBuyersSQL(connection, weekid) {

  returnArray = [];

  const request = new Request(
    `SELECT [name], prename 
    from [user] INNER JOIN buyertoweek
    on ([user].userid = buyertoweek.userid)
    WHERE weekid = '${weekid}';`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
        return results(error);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
        results(null, returnArray);
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      returnArray.push(column.value);
    });
  });

  connection.execSql(request);
}

/*
Die Funktion gibt die weekid, die mit der übergebenen übereinstimmt zurück.
*/
function getWeekEntrySQL(connection, weekid, results) {
  let returnArray = [];

  const request = new Request(
    `SELECT weekid from week
    WHERE weekid = '${weekid}';`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
        return results(error);
      } else {
        console.log(`${rowCount} row(s) returned`);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
        results(null, returnArray);
      }
    }
  );

  request.on("row", columns => {
    columns.forEach(column => {
      returnArray.push(column.value);
    });
  });

  connection.execSql(request);
}

function getMarkedEntriesSQL(connection, weekid, results) {

  returnArray = [];

  const request = new Request(
    `SELECT entrytoweek.entryid, userid, markDate FROM
    entrytoweek INNER JOIN buyertoentry ON (entrytoweek.entryid = buyertoentry.entryid)
    WHERE weekid = '${weekid}';`,
    (err, rowCount) => {
      if (err) {
        console.error(err.message);
        return results(err);
      } else {
        console.log(`${rowCount} row(s) returned`);
        results(null, returnArray);
        request.on('requestCompleted', () => {
          console.log('DONE!');
          connection.close();
        });
      }
    }
  );

  request.on("row", columns => {
    let returnLine = []
    columns.forEach(column => {
      returnLine.push(column.value);
    });
    returnArray.push(returnLine);
  });

  connection.execSql(request);
}

module.exports = { 
  addEntry, 
  getUsername,
  updateEntry, 
  deleteEntry, 
  markEntry,
  demarkEntry, 
  becomeBuyer, 
  becomeWorker, 
  getList,
  createWeek, 
  getBuyers,
  getWeekEntry,
  getMarkedEntries
};