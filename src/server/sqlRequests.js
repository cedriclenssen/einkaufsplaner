const pool = require('./connectionPool');


async function getList(weekid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`SELECT entryid, description, amount, unit, creationDate, name, prename, userid
      from entry NATURAL JOIN creatortoentry
      NATURAL JOIN user
      NATURAL JOIN entrytoweek 
      WHERE weekid = '${weekid}'`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function addEntry(description, amount, unit, userid, date, weekid) {
  let conn;
  let res;
  let res2;
  let res3;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`INSERT INTO entry (description, amount, unit) values('${description}', ${amount}, '${unit}');`);
    const entryid = res.insertId;
    res2 = await conn.query(`INSERT INTO creatortoentry values('${userid}', ${entryid}, '${date}');`);
    res3 = await conn.query(`INSERT INTO entrytoweek values(${entryid}, '${weekid}');`);
    // remove meta data from result array
    delete res.meta;
    delete res2.meta;
    delete res3.meta;
  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res + res2 + res3;
  }
}

async function getUsername(userid, password) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`SELECT userid, password from user where userid = '${userid}' AND password = '${password}'`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function getUserinfo(userid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`SELECT prename, name from user where userid = '${userid}'`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function updateEntry(entryid, description, amount, unit) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`UPDATE entry SET description = '${description}', amount = ${amount}, unit = '${unit}' 
    where entryid = '${entryid}';`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function deleteEntry(entryid) {
  let conn;
  let res, res2, res3, res4;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`DELETE FROM entrytoweek WHERE entryid = ${entryid}`);
    res2 = await conn.query(`DELETE FROM creatortoentry WHERE entryid = ${entryid}`);
    res3 = await conn.query(`DELETE FROM buyertoentry WHERE entryid = ${entryid}`);
    res4 = await conn.query(`DELETE FROM entry WHERE entryid = ${entryid}`);
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function markEntry(entryid, userid, markDate) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`INSERT INTO buyertoentry values('${userid}', '${entryid}', '${markDate}');`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function demarkEntry(entryid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`DELETE FROM buyertoentry WHERE entryid = '${entryid}';`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function becomeBuyer(userid, weekid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`INSERT INTO buyertoweek values('${userid}', '${weekid}');`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function becomeWorker(userid, weekid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`DELETE FROM buyertoweek where userid = '${userid}' AND weekid = '${weekid}';`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function createWeek(weekid, week, year) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`INSERT INTO week values('${weekid}', ${week}, ${year});`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function getBuyers(weekid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`SELECT userid, name, prename 
    from user NATURAL JOIN buyertoweek
    WHERE weekid = '${weekid}';`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function getWeekEntry(weekid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`SELECT weekid from week
    WHERE weekid = '${weekid}';`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

async function getMarkedEntries(weekid) {
  let conn;
  let res;
  try {
    conn = await pool.getConnection();
    res = await conn.query(`SELECT entrytoweek.entryid, user.userid, prename, name, markDate FROM
    entrytoweek NATURAL JOIN buyertoentry NATURAL JOIN user
    WHERE weekid = '${weekid}';`)
    // remove meta data from result array
    delete res.meta;

  } catch (err) {
    console.log(err);
  } finally {
    if (conn) {
      conn.end()
    }
    return res;
  }
}

module.exports = {
  addEntry,
  getUsername,
  updateEntry,
  deleteEntry,
  markEntry,
  demarkEntry,
  becomeBuyer,
  becomeWorker,
  getList,
  createWeek,
  getBuyers,
  getWeekEntry,
  getMarkedEntries,
  getUserinfo
};