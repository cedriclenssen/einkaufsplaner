
var express = require('express'); 
var path = require('path');
const sqlRequestSample = require('./src/server/sqlRequests.js');
const sqlmethods = require('./src/server/sqlmethods.js');

const app = express();
app.use(express.json());
// view engine setup
app.set('views', path.join('./src/client', 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(express.static('src/client/stylesheets'));
app.use(express.static('src/client/javascripts'));

app.get('/', function(req, res, next) {
    res.render('index');
});

app.put('/item/', async function(req, res, next) {
    await sqlRequestSample.addEntry(req.body.description, req.body.amount, req.body.unit, req.body.userid,  req.body.date, req.body.weekid);
    res.send('DONE');
});

app.get('/list/:weekid/', async function(req, res, next) {
   const response = await sqlRequestSample.getList(req.params.weekid);
   res.send(response);
});

app.put('/delete/', async function(req, res, next) {
    await sqlRequestSample.deleteEntry(req.body.entryid);
    res.send('DONE');
});

app.get('/week/:weekid/', async function(req, res, next) {
    const response = await sqlRequestSample.getWeekEntry(req.params.weekid);
    res.send(response);
});

app.put('/week/', async function(req, res, next) {
    await sqlRequestSample.createWeek(req.body.weekid, req.body.week, req.body.year);
    res.send('DONE');
});

app.put('/mark/', async function(req, res, next) {
    await sqlRequestSample.markEntry(req.body.entryid, req.body.userid, req.body.date);
    res.send('DONE');
});

app.put('/demark/', async function(req, res, next) {
    await sqlRequestSample.demarkEntry(req.body.entryid);
    res.send('DONE');
});

app.get('/marked/:weekid/', async function(req, res, next) {
    const response = await sqlRequestSample.getMarkedEntries(req.params.weekid);
    res.send(response);
});

app.post('/item/', async function(req, res, next) {
    await sqlRequestSample.updateEntry(req.body.entryid, req.body.description, req.body.amount, req.body.unit);
    res.send('DONE');
});

app.get('/buyerlist/:weekid/', async function(req, res, next) {
    const response = await sqlRequestSample.getBuyers(req.params.weekid);
    res.send(response);
});

app.put('/buyer/', async function(req, res, next) {
    await sqlRequestSample.becomeBuyer(req.body.userid, req.body.weekid);
    res.send('DONE');
});

app.put('/worker/', async function(req, res, next) {
    await sqlRequestSample.becomeWorker(req.body.userid, req.body.weekid);
    res.send('DONE');
});

app.get('/login/:username/:password/', async function(req, res, next) {
    const regexUsername = new RegExp(/^[a-zA-Z]+$/);
    !regexUsername.test(req.params.username) && res.send('invalid');
    const response = await sqlRequestSample.getUsername(req.params.username, req.params.password);
    const valid = checkCredentials(response);
    res.send(valid);
})

app.get('/userinfo/:userid/', async function(req, res, next) {
    const response = await sqlRequestSample.getUserinfo(req.params.userid);
    res.send(response);
})

function checkCredentials(usernameDBResponse) {
    if (usernameDBResponse[0] != undefined) {
        return 'valid';
    }
    return 'invalid';
}

module.exports = app;
